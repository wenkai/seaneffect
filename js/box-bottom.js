/**
 * Created by wenkai on 9/4/14.
 */

//基金排序
$('.box-order-types').each(function(){
    $(this).click(function(e){
        e.preventDefault();
        e.stopPropagation && e.stopPropagation();
        if($(this).hasClass('box-t2')){
            $(this).removeClass('box-t2').addClass('box-t1').parent().siblings().children().removeClass('box-t1').removeClass('box-t3').addClass('box-t2');
        }else if($(this).hasClass('box-t1')){
            $(this).removeClass('box-t1').addClass('box-t3');
        }else if($(this).hasClass('box-t3')){
            $(this).removeClass('box-t3').addClass('box-t1');
        }
    })
})
//右侧悬浮条二维码
$('.backtop .a3').hover(function(){
    $('.box-ewm-weixin').fadeIn();
},function(){
    $('.box-ewm-weixin').fadeOut();
});
$('.backtop .a5').hover(function(){
    $('.box-ewm-shouji').fadeIn();
},function(){
    $('.box-ewm-shouji').fadeOut();
});

//底部导航
$('.kstdmain > ul > li').each(function(i){
    if(i < 2){
        $(this).hover(function(){
            $(this).children('ul').stop(true,true).slideDown();
        },function(){
            $(this).children('ul').stop(true,true).slideUp();
        })
    }
});

//点击加减按钮，切换状态
var minusHoverd = false;
$('.box-btn-add').click(function(){
    if($(this).hasClass('minus')){
        $(this).removeClass('minus').children('p').hide();
    }else{
        minusHoverd = true;
        $(this).addClass('minus').children('p').slideDown();
        var t = this;
        setTimeout(function(){
            $(t).children('p').fadeOut();
        },1000);

        $(this).hover(function(){
            if(minusHoverd || !$(this).hasClass('minus')) return false;
            minusHoverd = true;
            $(this).children('p').stop(true,true).slideDown();
            var t = this;
            setTimeout(function(){
                $(t).children('p').fadeOut();
            },1000);
        },function(){
            minusHoverd = false;
        });
    }

    return false;
});


$('.box-relative').hover(function(){
    $(this).siblings().children('p').slideUp();
    $(this).children('p').stop(true,true).slideDown();
},function(){
    $(this).children('p').stop(true,true).slideUp();
})