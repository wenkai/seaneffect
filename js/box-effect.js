/**
 * Created by wenkai on 9/16/14.
 */
$('.box-hoveraddeffect').length && $('.box-hoveraddeffect').hover(function(){
    $(this).addClass('box-hovereffect');
},function(){
    $(this).removeClass('box-hovereffect');
});
$('.box-hoveraddeffectli').length && $('.box-hoveraddeffectli').hover(function(){
    $(this).children('p').eq(0).addClass('box-hovereffect');
},function(){
    $(this).children('p').eq(0).removeClass('box-hovereffect');
});
/*
$('.hotspro-1.box-hoveraddeffect').length && $('.hotspro-1.box-hoveraddeffect').each(function(i){
    var img = $(this).children('.hotspro_p2').children('img');
    var initurl = img.attr('src');
    $(this).hover(function(){
        var j = i+1;
        img.attr('src',initurl.replace('hotpro'+j+'.jpg','gif/hotpro'+j+'.gif'));
    },function(){
        img.attr('src',initurl);
    });
});
*/
(function(){
    var bigMenuClicked = false;


    $('.box-bigmenu-ctrl').click(function(){
        if(bigMenuClicked == 'animating') return false;

        var that = this;
        var w = bigMenuClicked == false ? 0 : 750;
        if(w){
            $('.box-bigmenu-ctrl .box-kefu,.box-kstz,.box-closemenu').stop(true,false).fadeIn();
        }
        bigMenuClicked = 'animating';

        if(!w){
            $(this).siblings('ul').animate({width:w+'px'},function(){

                bigMenuClicked = true;
                $('.box-bigmenu-ctrl .box-kefu,.box-kstz,.box-closemenu').stop(true,false).fadeOut(function(){
                    $(that).animate({height:'180px','margin-top':'-80px'}).parent('.kvblock').animate({'margin-top':'-100px'});
                    $(that).children('.box-minikefu').fadeIn();
                });

            });
        }else{
            $(that).children('.box-minikefu').fadeOut();
            $(that).animate({height:'256px','margin-top':'-156px'}).parent('.kvblock').animate({'margin-top':'0'},function(){
                $('.box-bigmenu-ctrl .box-kefu,.box-kstz,.box-closemenu').fadeIn();
                $(that).siblings('ul').animate({width:w+'px'});
                bigMenuClicked = false;
            });
        }

    });

    $('.box-hidebigmenu').click(function(){
        $('.kvblock').fadeOut();
        event.stopPropagation();
    });
})();
