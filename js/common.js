$(function(){
	//首次进入二维码显示 5s之后消失 
	var wechatHome = $("#wechatHome"),
		closeBtn = wechatHome.find(".close"),
		container = $(".topwarp .container");
	//首页进入自动
    
	if($("body").hasClass("pageIndex")){
		setTimeout(function(){
			wechatHome.addClass("show").slideDown(600, function(){
				//5s之后消失
				setTimeout(function(){
					wechatHome.removeClass("show").slideUp(600);
				}, 5000);
			});
		}, 1000);
	}
	
	//关闭按钮
	closeBtn.bind("click",function(){
		if(!wechatHome.hasClass("show")){
			return false;
		}
		wechatHome.removeClass("show").slideUp(600);
	});

	//点击头部二维码出来
	container.toggle(function(){
		wechatHome.addClass("show").slideDown(600);
	}, function(){
		wechatHome.removeClass("show").slideUp(600);
	});

	//search
	var logomenu = $("#logomenu"),
		searchParent = logomenu.find(".li1"),
		search = searchParent.find(".search");

	search.bind("focus",function(){
		searchParent.animate({"width":"120px"});
	}).bind("blur",function(){
		searchParent.animate({"width":"48px"});
	});


	//基金超市子菜单
	var marketMoney = $("#marketMoney");
		marketLi = logomenu.find(".market"),
		loginCon  = logomenu.find(".loginCon"),
		loginBtn = logomenu.find(".loginBtn");

	marketLi.hover(function(e){
		if(loginCon.hasClass("show")){
			loginCon.removeClass("show").fadeOut(300, function(){
				marketMoney.addClass("show").slideDown();
			});
		}else{
			marketMoney.addClass("show").slideDown();
		}
	});
    
    var menuA = logomenu.find('.ul2').children('li');
    menuA.click(function(){
        $(this).siblings().children('a').removeClass('on'); 
       $(this).children('a').addClass('on') ;
    }).hover(function(){
        if(!$(this).hasClass("market")){
            marketMoney.removeClass("show").slideUp();
        }
    });

	logomenu.hover(function(){}, function(){
		marketMoney.removeClass("show").slideUp();
	});

    var marketDetail = $('.marketDetail .list');
    marketMoney.find('.submenu').each(function(i){
        $(this).hover(function(){
            $(this).addClass('active').siblings().removeClass('active');
            marketDetail.hide().eq(i).show();
        })
    });

    
    //登录窗口提示文字 兼容ie678
	var userName = loginCon.find(".card"),
		pasd = loginCon.find(".pasd"),
		tips =  loginCon.find(".tips");
        
	loginBtn.bind("click", function(){
		if(loginCon.hasClass("show")){
			loginCon.removeClass("show").slideUp();
            pasd.val('');
		}else{
			if(marketMoney.hasClass("show")){
				marketMoney.removeClass("show").slideUp(300, function(){
					loginCon.addClass("show").slideDown();
				});
			}else{
				loginCon.addClass("show").slideDown();
			}
			//for IE6
			tips.addClass("show").show();

		}
	});

	
        
        
	//点击body登录框消失
	$('body').bind("click", function(e){
		var _this = $(e.target);
		if(_this.hasClass("loginBtn") || _this.parents().hasClass("loginCon") || _this.hasClass("loginCon") || !loginCon.hasClass("show")){
			//return false;
		}else{
		  loginCon.removeClass("show").slideUp();
          pasd.val('');
		}

		
	});


	userName.bind("focus",function(){
		if(userName.val() == "证件号"){
			userName.removeClass("n").val("");
		}
	}).bind("blur", function(){
		if(userName.val() == ""){
			userName.addClass("n").val("证件号");
		}
	});
	
	pasd.bind("focus",function(){
		if(tips.hasClass("show")){
			tips.removeClass("show").hide();
		}
	}).bind("blur", function(){
		if(pasd.val() == ""){
			tips.addClass("show").show();
		}
	});

	tips.bind("click",function(){
		tips.removeClass("show").hide();
		pasd.focus();
	});
	//bar 可以拖动
	var processBar = $(".processBar"),
	 	bar = processBar.find(".bar"),
	 	item = $(".funds_style .item"),
	 	canDrag = false,
	 	startPageY = 0,
	 	top = 0, 
	 	p = 0,
	 	_index = 0,
	 	con, 
	 	diff = 0, 
	 	isAnima = false;
	 var key = {"one": 34, "two": 68, "three": 68, "four": 68, "five": 41}
	 bar.bind("mousedown", function(e){
	 	if (isAnima) {return false};
	 	canDrag = true;
	 	top = parseInt($(this).attr("top"));
	 	startPageY = e.pageY;
	    p = $(this).parent(".processBar"),
		_index = p.attr("data-index");
		con = $(this);
	 }).bind("mouseup", function(){
	 	canDrag = false;
	 	dragEndEvent(con, top + diff);
	 });

   
	 item.bind("mouseleave mouseup", function(){
	 	if(canDrag){
		 	dragEndEvent(con, top + diff);
		 	canDrag = false;
		}
	 }).bind("mousemove", function(e){
	   e.preventDefault();
       e.stopPropagation && e.stopPropagation();
	 	if(canDrag){
	 	    diff = e.pageY - startPageY;
	 	    var tot = top + diff;
	 	    if(tot <= 0){
	 	    	return false;
	 	    }
	 	    if(tot > 205){
	 	    	tot = 205;
	 	    }
            tot += 2;
	 		con.css({"top" : tot + "px"}).attr("top", tot);
	 		con.parents("li").find(".on").removeClass("on");
 			var current = (Math.floor(tot / key[_index]) + 1);
 			con.parents("li").find(".l_"+ current).addClass("on");
	 	}
	 });

	 function dragEndEvent(con, diff){
	 	isAnima = true;
	 	var distance = 0, count = 0;
	 	switch(_index){
	 		case "one":
	 			count = 6;
	 			distance = 34;
	 		break;
	 		case "two":
	 			count = 3;
	 			distance = 68;
	 		break;
	 		case "three":
	 			count = 3;
	 			distance = 68;
	 		break;
	 		case "four":
	 			count = 3;
	 			distance = 68;
	 		break;
	 		case "five":
	 			count = 5;
	 			distance = 41;
	 		break;
	 	}

	 	var _diff = (diff % distance) > (distance / 2) ? 1 : 0;
        
        var tot = Math.min((Math.floor(diff / distance) + _diff) * distance, count*distance);
        if(tot <= 0){
 	    	tot = 0;
 	    }
 	    if(tot > 205){
 	    	tot = 205;
 	    }
        tot += 2;
	 	con.animate({"top" : tot + "px"}, function(){
			isAnima = false;
		}).attr("top", tot);
		con.parents("li").find(".on").removeClass("on");
		var current = Math.min((Math.floor(diff / distance) + _diff + 1), count + 1);
		con.parents("li").find(".l_"+ current).addClass("on");
	 }

	 item.delegate("li", "click", function(e){
	 	var _this = $(this);
	 		_thisIndex = _this.attr("index"),
	 		processBar =_this.parents(".item").find(".processBar"),
	 		con = processBar.find(".bar")
	 		_index = processBar.attr("data-index");

	 	var distance = 0, count = 0;
	 	switch(_index){
	 		case "one":
	 			distance = 34;
	 		break;
	 		case "two":
	 			distance = 68;
	 		break;
	 		case "three":
	 			distance = 68;
	 		break;
	 		case "four":
	 			distance = 68;
	 		break;
	 		case "five":
	 			distance = 41;
	 		break;
	 	}
        
        var top = distance * (parseInt(_thisIndex) - 1);
        if(top <= 0){
 	    	top = 0;
 	    }
 	    if(top > 205){
 	    	top = 205;
 	    }
        top += 2;
	 	con.animate({"top" : top + "px"}, function(){
			isAnima = false;
		}).attr("top", top);
		_this.parent().find(".on").removeClass("on");
		_this.addClass("on");
	 });
});